//
//  SecondViewController.swift
//  UIElementPractise
//
//  Created by Nheng Vanchhay on 11/13/20.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textFieldShow: UITextView!
    var dataShow: String?
    var colorPic: UIColor?
    var image: UIImage?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        imageView.tintColor = colorPic
        imageView.image = image
        textFieldShow.text = dataShow
        textFieldShow.isEditable = false
    }

    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
extension SecondViewController: SendDataDelegate{
    func sentData(userName: String, address: String, email: String, phoneNumber: String, colorPic: UIColor, image: UIImage) {
        dataShow = "Name: \(userName) \nAddress: \(address) \nEmail: \(email) \nPhone Number: \(phoneNumber)"
        self.colorPic = colorPic
        self.image = image
    }
}
