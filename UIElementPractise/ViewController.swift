//
//  ViewController.swift
//  UIElementPractise
//
//  Created by Nheng Vanchhay on 11/13/20.
//

import UIKit

protocol SendDataDelegate {
    func sentData(userName: String, address: String, email: String, phoneNumber: String, colorPic: UIColor, image: UIImage)
}
class ViewController: UIViewController {

    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtUserName: UITextField!
    
    @IBOutlet weak var imageView: UIImageView!
    var delegate: SendDataDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtUserName.delegate = self
        txtEmail.delegate = self
        txtAddress.delegate = self
        txtPhoneNumber.delegate = self
        txtPhoneNumber.returnKeyType = .done
//        txtPhoneNumber.keyboardType = .phonePad
        txtEmail.keyboardType = .emailAddress
    
    }
    
    
    @IBAction func btnSend(_ sender: Any) {
        let userName = txtUserName.text!
        let address = txtAddress.text!
        let email = txtEmail.text!
        let phoneNumber = txtPhoneNumber.text!
        let color = imageView.tintColor!
        let image = imageView.image!
        
        let secondView = storyboard?.instantiateViewController(identifier: "sencondView") as! SecondViewController
        secondView.modalPresentationStyle = .fullScreen
        self.delegate = secondView
        delegate?.sentData(userName: userName , address: address, email: email, phoneNumber: phoneNumber, colorPic: color, image: image)
        txtUserName.text = ""
        txtAddress.text = ""
        txtPhoneNumber.text = ""
        txtEmail.text = ""
        
        present(secondView, animated: true, completion: nil)
    }
    
}
extension ViewController: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            
            nextField.becomeFirstResponder()
         } else {
            textField.resignFirstResponder()
         }
         return false
    }
    
}

